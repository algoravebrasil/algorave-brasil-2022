Algorave Brasil 2022

- Quando: 26 nov 2022 (sábado)
- Onde: Streaming online + Comunidades locais organizadas

Artistas, performances, palestras e oficinas

Hora, Nome, Cidade, Descrição da atividade
Nota: Horário de Brasília (UTC -3)
- 00:00
- 00:30
- 01:00
- 01:30
- 02:00
- 02:30
- 03:00
- 03:30
- 04:00
- 04:30
- 05:00
- 05:30
- 06:00
- 06:30
- 07:00
- 07:30
- 08:00
- 08:30
- 09:00
- 09:30
- 10:00 Joenio e convidados, Paris, Papo de boas-vindas a Algorave Brasil 2022
- 10:30 Joenio e convidados, Paris, Papo de boas-vindas a Algorave Brasil 2022
- 11:00 Joenio, Paris, Performance, Hrung+TidalCycles+LeBiniou
- 11:30 Bruno Gola + Berlin (evento local)
- 12:00 Bruno Gola + Berlin... 
- 12:30 Bruno Gola + 
- 13:00 berin, Berlin, performance
- 13:30 smosgasbord, Costa Rica, orca + supercollider
- 14:00 Gil Fuser, SP. SuperCollider JitLib & Tidal
- 14:30 Igor Medeiros & Gil Fuser
- 15:00 Indizível, Salvador, performance
- 15:30 luizio, Belo Horizonte, performance
- 16:00 Ángel Jara, Buenos Aires, performance Streaming Online 
- 16:30 batata, SP, performance
- 17:00 lowbin (Victor Hugo), Brasília, Performance 
- 17:30 porres, são paulo, performance
- 18:00 A1219, São Paulo, performance
- 18:30 diegodukao, Rio de Janeiro, performance
- 19:00 luczan, Porto Alegre, performance
- 19:30 luczan, Porto Alegre, performance
- 20:00 luczan, Porto Alegre, performance 
- 20:30 euFraktus_X, Brasilia, performance
- 21:00 BSBLOrk, Brasilia, performance
- 21:30 BSBLOrk, Brasilia, performance
- 22:00
- 22:30
- 23:00 
- 23:30 fracto, Recife, performance com Hydra

Comunidades locais organizadas

- Cidade, Local, Horario, Organizadores
- Paris, <local nao definido>, <horario nao definido>, Joenio
- Berlin, <local nao definido>, <horario nao definido>, berin + Bruno Gola
